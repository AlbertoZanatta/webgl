window.onload = init;

//Global gl and program variables
var gl = null;
var program = null;

function init() {
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    gl.enable(gl.DEPTH_TEST);

    //Load program shaders 
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Enable depth testing
    gl.enable(gl.DEPTH_TEST);

    //Set the vertices for the ground quad
    var vertices = [
        vec4(-4, -1, -1, 1),
        vec4(-4, -1, -21, 1),
        vec4(4, -1, -1, 1),
        vec4(4, -1, -21, 1)     
    ];
    
    //Set up the vertex coordinates to be received as an attribute in the vertex shader
    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Create the texture object for the ground
    var texture = gl.createTexture();

    //Generate a 64x64 RGBA texture which represents a black and white checkboard
    var texSize = 64;
    var numRows = 8;
    var numCols = 8;

    var myTexels = new Uint8Array(4 * texSize * texSize);

    for (var i = 0; i < texSize; i++) {
        for (var j = 0; j < texSize; j++) {
            var patchx = Math.floor(i / (texSize / numRows));
            var patchy = Math.floor(j / (texSize / numCols));

            var c = (patchx % 2 !== patchy % 2 ? 255 : 0);

            myTexels[4 * i * texSize + 4 * j] = c;
            myTexels[4 * i * texSize + 4 * j + 1] = c;
            myTexels[4 * i * texSize + 4 * j + 2] = c;
            myTexels[4 * i * texSize + 4 * j + 3] = 255;

        }
    }

    //Enable texture unit 0
    gl.activeTexture(gl.TEXTURE0);

    //Bind the texture object 
    gl.bindTexture(gl.TEXTURE_2D, texture);

    //Set the checkboard image  to be used with the currently bound 2D texture
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, texSize, texSize, 0, gl.RGBA, gl.UNSIGNED_BYTE, myTexels);

    //Set the texture parameters
    //1. Set the wrap mode to repeat (for both s and t coordinates)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

    //2. Set the filter mode to nearest (for both handling magnification and minifications issues)
    //(No need for generating the MipMaps as gl.NEAREST automatically disables MipMapping for the TEXTURE_MIN_FILTER)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

    //Set the texture linked to texture unit 0 as the sampler2D in the fragment shader
    gl.uniform1i(gl.getUniformLocation(program, "texMap"), 0);

    //Pupulate the texture coordinates array
    var texCoord = [
        vec2(-1.5, 0.0),
        vec2(-1.5, 10.0),
        vec2(2.5, 0.0),
        vec2(2.5, 10.0)   
    ]

    //Set up the texture coordinates to be received as an attribute in the vertex shader
    var tBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, tBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(texCoord), gl.STATIC_DRAW);

    var vTexCoord = gl.getAttribLocation(program, "vTexCoord");
    gl.vertexAttribPointer(vTexCoord, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vTexCoord);

    render();
}

function render() {

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Define the projection matrix
    var projectionMatrix = mat4();
    var fovy = 90;
    var aspect = 1;
    var near = 0.001;
    var far = 100;
    projectionMatrix = perspective(fovy, aspect, near, far);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    //Define the model-view matrix
    var modelViewMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelViewMatrix"), false, flatten(modelViewMatrix));

    //Draw the textured quad
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}

