window.onload = function () {
    setMenus();
    init();
}

//Global gl and program variables
var gl = null;
var program = null;

//Wrap modes and filterModes arrays;
var wrapModes = [];  
var filterModesMag = [];
var filterModesMin = [];

function init() {
    canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    gl.enable(gl.DEPTH_TEST);

    // Load program shaders 
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Set the vertices for the ground quad
    var vertices = [
        vec4(-4, -1, -1, 1),
        vec4(-4, -1, -21, 1),
        vec4(4, -1, -1, 1),
        vec4(4, -1, -21, 1)     
    ];

    //Set up the vertex coordinates to be received as an attribute in the vertex shader
    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Create the texture object for the ground
    var texture = gl.createTexture();

    //Generate a 64x64 RGBA texture which represents a black and white checkboard
    var texSize = 64;
    var numRows = 8;
    var numCols = 8;

    var myTexels = new Uint8Array(4 * texSize * texSize);

    for (var i = 0; i < texSize; i++) {
        for (var j = 0; j < texSize; j++) {
            var patchx = Math.floor(i / (texSize / numRows));
            var patchy = Math.floor(j / (texSize / numCols));

            var c = (patchx % 2 !== patchy % 2 ? 255 : 0);

            myTexels[4 * i * texSize + 4 * j] = c;
            myTexels[4 * i * texSize + 4 * j + 1] = c;
            myTexels[4 * i * texSize + 4 * j + 2] = c;
            myTexels[4 * i * texSize + 4 * j + 3] = 255;

        }
    }

    //Enable texture unit 0
    gl.activeTexture(gl.TEXTURE0);

    //Bind the texture object 
    gl.bindTexture(gl.TEXTURE_2D, texture);

    //Set the checkboard image  to be used with the currently bound 2D texture
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, texSize, texSize, 0, gl.RGBA, gl.UNSIGNED_BYTE, myTexels);

    //Populate wrap modes and filter modes arrays;
    wrapModes = [
        gl.REPEAT,
        gl.CLAMP_TO_EDGE
    ];

    filterModesMag = [
        gl.NEAREST,
        gl.LINEAR,
    ];

    filterModesMin = [
        gl.NEAREST,
        gl.LINEAR,
        gl.NEAREST_MIPMAP_NEAREST,
        gl.LINEAR_MIPMAP_LINEAR,
        gl.NEAREST_MIPMAP_LINEAR,
        gl.LINEAR_MIPMAP_NEAREST
    ];

    //Set the texture parameters
    //1. Set the wrap mode to repeat (for both s and t coordinates)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrapModes[0]);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrapModes[0]);

    //As the user can select minification techniques which make use of MipMap textures, it is necessary to generate MipMap textures beforehand
    gl.generateMipmap(gl.TEXTURE_2D);

    //2. Set the filter mode to nearest (for both handling magnification and minifications issues 
    // - even though the default value for the minification method should be gl.NEAREST_MIPMAP_LINEAR)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filterModesMag[0]);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filterModesMin[0]);

    //Set the texture linked to texture unit 0 as the sampler2D in the fragment shader
    gl.uniform1i(gl.getUniformLocation(program, "texMap"), 0);

    //Populate the texture coordinates array
    var texCoord = [
        vec2(-1.5, 0.0),
        vec2(-1.5, 10.0),
        vec2(2.5, 0.0),
        vec2(2.5, 10.0)
    ]

    //Set up the texture coordinates to be received as an attribute in the vertex shader
    var tBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, tBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(texCoord), gl.STATIC_DRAW);

    var vTexCoord = gl.getAttribLocation(program, "vTexCoord");
    gl.vertexAttribPointer(vTexCoord, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vTexCoord);

    //Define, set and bind the model-view matrix and the projection matrix
    //1. Model-view matrix
    var modelViewMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelViewMatrix"), false, flatten(modelViewMatrix));
    //2. Projection matrix
    var projectionMatrix = mat4();
    var fovy = 90;
    var aspect = 1;
    var near = 0.001;
    var far = 100;
    var perspectiveMatrix = perspective(fovy, aspect, near, far);
    projectionMatrix = mult(projectionMatrix, perspectiveMatrix);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    render();
}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    //Draw the textured quad
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}

function setMenus()
{
    //1. select wrap mode
    var wmm = document.getElementById("wrap_mode_menu");
    wmm.addEventListener("click", function () {
        var wrapModeIndex = wmm.selectedIndex;
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrapModes[wrapModeIndex]);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrapModes[wrapModeIndex]);
        render();
    });

    //2. select magnification method
    var fmmag = document.getElementById("filter_mode_mag");
    fmmag.addEventListener("click", function () {
        var filterMagIndex = fmmag.selectedIndex;
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filterModesMag[filterMagIndex]);
        render();
    });

    //3. select minification method
    var fmmin = document.getElementById("filter_mode_min");
    fmmin.addEventListener("click", function () {
        var filterMinIndex = fmmin.selectedIndex;
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filterModesMin[filterMinIndex]);
        render();
    });
}

