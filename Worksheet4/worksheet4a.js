window.onload = function () {
    init();
    setButtons();
}

//Array of vertices
var vertices = [];

//Vertex array attribute buffer
var vBuffer;

//Global variables for drawing the sphere using recursive subdivisions
var numTimesToSubdivide = 4;
var va = vec4(0.0, 0.0, 1.0, 1);
var vb = vec4(0.0, 0.942809, -0.333333, 1);
var vc = vec4(-0.816497, -0.471405, -0.333333, 1);
var vd = vec4(0.816497, -0.471405, -0.333333, 1);

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl)
    {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    //Enable depth testing
    gl.enable(gl.DEPTH_TEST);

    //Calculate the vertices of a unit sphere using recursing subdivision
    tetrahedron(va, vb, vc, vd, numTimesToSubdivide);

    // Load shaders 
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Vertex array attribute buffer
    vBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Set the model-view matrix and the projection matrix

    //Set the projection (perspective) matrix 
    var projectionMatrix = mat4();
    var fovy = 45;
    var aspect = 1;
    var near = 0.01;
    var far = 10;
    projectionMatrix = perspective(fovy, aspect, near, far);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    //Set the model-view matrix
    var modelViewMatrix = mat4();
    eye = vec3(3, 3, 3);
    at = vec3(0.0, 0.0, 0.0);
    up = vec3(0.0, 1.0, 0.0); 
    modelViewMatrix = mult(modelViewMatrix, lookAt(eye, at, up));
    gl.uniformMatrix4fv( gl.getUniformLocation(program, "modelViewMatrix"), false, flatten(modelViewMatrix));
  
    updateNumText();
    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    gl.drawArrays(gl.TRIANGLES, 0, vertices.length);

}

//helper functions to draw the 1 unit sphere
function tetrahedron(a, b, c, d, n)
{
    divideTriangle(a, b, c, n);
    divideTriangle(d, c, b, n);
    divideTriangle(a, d, b, n);
    divideTriangle(a, c, d, n);
}

function divideTriangle(a, b, c, count)
{
    if (count > 0)
    {
        var ab = normalize(mix(a, b, 0.5), true);
        var ac = normalize(mix(a, c, 0.5), true);
        var bc = normalize(mix(b, c, 0.5), true);

        divideTriangle(a, ab, ac, count - 1);
        divideTriangle(ab, b, bc, count - 1);
        divideTriangle(bc, c, ac, count - 1);
        divideTriangle(ab, bc, ac, count - 1);
    }
    else
    {
        triangle(a, b, c);
    }
}

function triangle(a, b, c)
{
    vertices.push(a);
    vertices.push(b);
    vertices.push(c);
}

//function to set the buttons' functionalities
function setButtons()
{
    //1. button to increase the number of subdivisions
    document.getElementById("plus_button").onclick = function () {
        numTimesToSubdivide += 1;
        vertices = [];

        tetrahedron(va, vb, vc, vd, numTimesToSubdivide);

        gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

        updateNumText();
        render();
    };

    //2. button to decrease the number of subdivisions
    document.getElementById("minus_button").onclick = function () {
        numTimesToSubdivide -= 1;
        vertices = [];

        tetrahedron(va, vb, vc, vd, numTimesToSubdivide);

        gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

        updateNumText();
        render();
    };
}
function updateNumText()
{
    document.getElementById("num_subdivisions").textContent = numTimesToSubdivide;
}


