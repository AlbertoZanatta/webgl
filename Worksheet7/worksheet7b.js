window.onload = init;
//Global gl and program variables
var gl = null;
var program = null;

//Angle for the rotation of the light source
var theta = 0;
//Dynamic light source in the scene
var light;
//Projection matrix onto plane y = -1
var M;

function init() {
    canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }
    
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    //Enable depth testing 
    gl.enable(gl.DEPTH_TEST);

    //Load program shaders 
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Set the vertices for the perspective planes;
    var planes_vertices = [
        //1. Ground plane
        vec4(2, -1, -1, 1),
        vec4(2, -1, -5, 1),
        vec4(-2, -1, -1, 1),
        vec4(-2, -1, -5, 1),

        //2. Parallel quad
        vec4(0.75, -0.5, -1.25, 1),
        vec4(0.75, -0.5, -1.75, 1),
        vec4(0.25, -0.5, -1.25, 1),
        vec4(0.25, -0.5, -1.75, 1),

        //3. Perpendicular quad
        vec4(-1, -1, -3, 1),
        vec4(-1, 0, -3, 1),
        vec4(-1, -1, -2.5, 1),
        vec4(-1, 0, -2.5, 1)
    ];

    //Set up the vertex coordinates to be received as an attribute in the vertex shader
    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(planes_vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Create the texture object for the ground
    var texture0 = gl.createTexture();

    //Create an image object
    var image = new Image();
    image.crossorigin = 'anonymous';
    //Register an event handler to be called when the image loading completes
    image.onload = function () {
        //Flip the image's y axis
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        //Enable texture unit 0
        gl.activeTexture(gl.TEXTURE0);
        //Bind the texture object to the target
        gl.bindTexture(gl.TEXTURE_2D, texture0);
        //Set the texture image
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        //Set the texture parameters
        //1. Set the wrap mode to repeat (for both s and t coordinates)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

        //Generate mipmaps
        gl.generateMipmap(gl.TEXTURE_2D);

        //2. Set the filter modes
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);

        render();
    };

    //Tell the browser to load an image
    image.src = 'xamp23.png';

    //Create the red texture object for the two smaller quads
    var texture1 = gl.createTexture();
    //Enable texture unit 1
    gl.activeTexture(gl.TEXTURE1);
    //Bind the texture object to the target
    gl.bindTexture(gl.TEXTURE_2D, texture1);
    // Fill the texture with a 1x1 red pixel.
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([255, 0, 0, 255]));

    //Populate the texture coordinates array
    var texCoord = [
        //1. Ground plane
        vec2(1.0, 0.0),
        vec2(1.0, 1.0),
        vec2(0.0, 0.0),
        vec2(0.0, 1.0),

        //2. Parallel quad
        vec2(1.0, 0.0),
        vec2(1.0, 1.0),
        vec2(0.0, 0.0),
        vec2(0.0, 1.0),

        //3. Perpendicular quad
        vec2(1.0, 0.0),
        vec2(1.0, 1.0),
        vec2(0.0, 0.0),
        vec2(0.0, 1.0)
    ]

    //Set up the texture coordinates to be received as an attribute in the vertex shader
    var tBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, tBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(texCoord), gl.STATIC_DRAW);

    var vTexCoord = gl.getAttribLocation(program, "vTexCoord");
    gl.vertexAttribPointer(vTexCoord, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vTexCoord);

   
    //Initial position of the light source (the light will move dynamically around a circle with center on (0, 2, -2) and radius 2
    light = vec3(0 + 2 * Math.cos(theta), 2.0, -2 + 2 * Math.sin(theta));

    //Calculate all the matrices that will remain the same throughout the program and the calls to the render() function,
    //so that it's not necessary to re-create them every time the render function is called. 

    //1. Matrix for the shadow projection
    //Set the shadow - projection matrix initially as an identity matrix
    M = mat4(); 
    M[3][3] = 0;
    //Subtract the y coordinate of the plane y = -1 - and therefore add 1 - to the y coordinate of the light position 
    //(which will remain the same even if the light is moving on a circle) to achieve a projection onto such plane;
    M[3][1] = -1.0 / (light[1] + 1); 

    //2. Projection matrix 
    var projectionMatrix = mat4();
    var fovy = 90;
    var aspect = 1;
    var near = 0.001;
    var far = 100;
    var perspectiveMatrix = perspective(fovy, aspect, near, far);
    projectionMatrix = mult(projectionMatrix, perspectiveMatrix);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    //3. View matrix
    var viewMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "viewMatrix"), false, flatten(viewMatrix));


    //4. Model matrix (common to all the quads in the scene)
    var modelMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelMatrix"), false, flatten(modelMatrix));
}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT |gl.DEPTH_BUFFER_BIT);

    //Calculate the updated position of the light source
    light[0] = 0 + 2 * Math.cos(theta);
    light[2] = -2 + 2 * Math.sin(theta);

    //1. Draw the ground plane
    //Set the texture linked to texture unit 0 as the sampler2D in the fragment shader
    gl.uniform1i(gl.getUniformLocation(program, "texMap"), 0);
    //Set the shadow matrix as the identity matrix
    var shadowMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "shadowMatrix"), false, flatten(shadowMatrix));
    //Draw the ground
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    //2. Draw the shadows
    //Calculate and bind the 'shadowMatrix' for the projected shadows
    shadowMatrix = mult(shadowMatrix, translate(light[0], light[1], light[2]));  //translate everything back to previous woorld coordinates
    shadowMatrix = mult(shadowMatrix, M); //projection of the vertices onto the plane y = -1
    shadowMatrix = mult(shadowMatrix, translate(-light[0], -light[1], -light[2])); //translation to get the light source position at the center of the world coordinates
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "shadowMatrix"), false, flatten(shadowMatrix));
    //Set the texture linked to texture unit 1 as the sampler2D in the fragment shader
    gl.uniform1i(gl.getUniformLocation(program, "texMap"), 1);
    //Drawing the shadows
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);
    gl.drawArrays(gl.TRIANGLE_STRIP, 8, 4);

    //3. Draw the smaller quads
    //Set the shadow matrix as the identity matrix
    var shadowMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "shadowMatrix"), false, flatten(shadowMatrix));

    //Draw the parallel quad
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);

    //Draw the perpendicular quad
    gl.drawArrays(gl.TRIANGLE_STRIP, 8, 4);

    //Slightly increase the angle of rotation of the light source
    theta += 0.01;

    requestAnimFrame(render);
}

 