// JavaScript source code
window.onload = init;

function init()
{
    var canvas = document.getElementById("canv");

    var gl = WebGLUtils.setupWebGL(canvas);

    if (!gl) {
        console.log("Error in initializing the canvas");
        return;
    }
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT);
}
