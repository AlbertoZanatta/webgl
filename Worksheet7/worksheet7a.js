window.onload = init;

//Global gl and program variables
var gl = null;
var program = null;

function init() {
    canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    //Enable depth testing and face culling
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);

    //Load program shaders 
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Set the vertices for the perspective planes
    var planes_vertices = [
        //1. Ground 
        vec4(2, -1, -1, 1),
        vec4(2, -1, -5, 1),
        vec4(-2, -1, -1, 1),
        vec4(-2, -1, -5, 1),

        //2. Parallel quad
        vec4(0.75, -0.5, -1.25, 1),
        vec4(0.75, -0.5, -1.75, 1),
        vec4(0.25, -0.5, -1.25, 1),
        vec4(0.25, -0.5, -1.75, 1),

        //3. Perpendicular quad
        vec4(-1, -1, -3, 1),
        vec4(-1, 0, -3, 1),
        vec4(-1, -1, -2.5, 1),
        vec4(-1, 0, -2.5, 1)
    ];


    //Set up the vertex coordinates to be received as an attribute in the vertex shader
    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(planes_vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Create the texture object for the ground
    var texture0 = gl.createTexture();

    //Create an image object
    var image = new Image();
    image.crossorigin = 'anonymous';
    //Register an event handler to be called when the image loading completes
    image.onload = function () {
        //Flip the image's y axis
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        //Enable texture unit 0
        gl.activeTexture(gl.TEXTURE0);
        //Bind the texture object to the target
        gl.bindTexture(gl.TEXTURE_2D, texture0);
        //Set the texture image
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        //Set the texture parameters
        //1. Set the wrap mode to repeat (for both s and t coordinates)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

        //Generate mipmaps
        gl.generateMipmap(gl.TEXTURE_2D);

        //2. Set the filter modes
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);

        render();
    };

    //Tell the browser to load an image
    image.src = 'xamp23.png';

    //Create the red texture object for the two smaller quads
    var texture1 = gl.createTexture();
    //Enable texture unit 1
    gl.activeTexture(gl.TEXTURE1);
    //Bind the texture object to the target
    gl.bindTexture(gl.TEXTURE_2D, texture1);
    // Fill the texture with a 1x1 red pixel.
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([255, 0, 0, 255]));

    //Populate the texture coordinates array
    var texCoord = [
        //1. Ground plane
        vec2(1.0, 0.0),
        vec2(1.0, 1.0),
        vec2(0.0, 0.0),
        vec2(0.0, 1.0),
       
        //2. Parallel quad
        vec2(1.0, 0.0),
        vec2(1.0, 1.0),
        vec2(0.0, 0.0),
        vec2(0.0, 1.0),

        //3. Perpendicular quad
        vec2(1.0, 0.0),
        vec2(1.0, 1.0),
        vec2(0.0, 0.0),
        vec2(0.0, 1.0)
    ]

    //Set up the texture coordinates to be received as an attribute in the vertex shader
    var tBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, tBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(texCoord), gl.STATIC_DRAW);

    var vTexCoord = gl.getAttribLocation(program, "vTexCoord");
    gl.vertexAttribPointer(vTexCoord, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vTexCoord);
}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    var projectionMatrix = mat4();
    var fovy = 90;
    var aspect = 1;
    var near = 0.001;
    var far = 5;
    var perspectiveMatrix = perspective(fovy, aspect, near, far);
    projectionMatrix = mult(projectionMatrix, perspectiveMatrix);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    var modelViewMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelViewMatrix"), false, flatten(modelViewMatrix));

    //1. Draw the ground plane
    //Set the texture linked to texture unit 0 as the sampler2D in the fragment shader
    gl.uniform1i(gl.getUniformLocation(program, "texMap"), 0);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    //2. Draw the parallel quad
    //Set the texture linked to texture unit 1 as the sampler2D in the fragment shader
    gl.uniform1i(gl.getUniformLocation(program, "texMap"), 1);
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);

    //3. Draw the perpendicular quad
    gl.drawArrays(gl.TRIANGLE_STRIP, 8, 4);
}

 