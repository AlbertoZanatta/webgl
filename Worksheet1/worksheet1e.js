window.onload = init;

var vertices = [];
var translation = 0;

function init() {
    canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    // Load shaders and initialize attribute buffers
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //center of the bouncing circle
    vertices = [
        vec4(0, 0, 0, 1)
    ];

    //radius of the circle
    var radius = 0.5;

    //generating 360 points laying on the circle
    var angle = 0;
    var precision = 0.05;
    for (; angle < 2*Math.PI + precision; angle += precision)
    {
        x = radius * Math.cos(angle);
        y = radius * Math.sin(angle);
        vertices.push(vec4(x, y, 0, 1));
    }


    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    render();
}


function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    var translationMatrix = translate(0, triangularWave(translation), 0);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "translationMatrix"), false, flatten(translationMatrix));
    gl.drawArrays(gl.TRIANGLE_FAN, 0, vertices.length);

    translation += 0.01;
    requestAnimationFrame(render);
}


function triangularWave(x) {
    return (((2.0 / Math.PI) * Math.asin(Math.sin(Math.PI * x)))) * 0.5;
}
