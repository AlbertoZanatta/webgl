window.onload = function () {
    init();
    setButtons();
}
//Global variables gl and program
var gl = null;
var program = null;

//Vertex and normal attributes
var vertices = [];
var normals = [];

//Vertex array attribute buffer
var vBuffer;
//Normal array attribute buffer
var nBuffer;

//Global variables for drawing the sphere using recursive subdivisions
var numTimesToSubdivide = 4;
var va = vec4(0.0, 0.0, 1.0, 1);
var vb = vec4(0.0, 0.942809, -0.333333, 1);
var vc = vec4(-0.816497, -0.471405, -0.333333, 1);
var vd = vec4(0.816497, -0.471405, -0.333333, 1);

//angle for the rotating camera
var theta = 0;

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl)
    {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    //Enable depth testing and face culling
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);

    //Calculate the vertices of a unit sphere using recursing subdivision
    tetrahedron(va, vb, vc, vd, numTimesToSubdivide);

    // Load shaders 
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Set and bind material properties and light direction 
    var lightDirection = vec4(0.0, 0.0, -1.0, 0.0);
    gl.uniform4fv(gl.getUniformLocation(program, "lightDirection"), flatten(lightDirection));
    var materialDiffuse = vec4(0.8, 0.8, 0.8, 1.0);
    var lightDiffuse = vec4(1.0, 1.0, 1.0, 1.0);
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);
    gl.uniform4fv(gl.getUniformLocation(program, "diffuseProduct"), flatten(diffuseProduct));

    //Normal array attribute buffer
    nBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, nBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW); 

    var vNormal = gl.getAttribLocation(program, "vNormal");
    gl.vertexAttribPointer(vNormal, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vNormal);

    //Vertex array attribute buffer
    vBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Set the model matrix
    modelMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelMatrix"), false, flatten(modelMatrix));


    //Set the projection (perspective) matrix 
    var projectionMatrix = mat4();
    var fovy = 45;
    var aspect = 1;
    var near = 0.01;
    var far = 10;
    projectionMatrix = perspective(fovy, aspect, near, far);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    updateNumText();

    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    var viewMatrix = mat4();
    //Rotate the camera around a circle with radius 4 centered on (0.0, 0.0, 0.0)
    eyeX = 0.0 + 4.0 * Math.cos(theta);
    eyeZ = 0.0 + 4.0 * Math.sin(theta);
    eye = vec3(eyeX, 0.0, eyeZ);
    at = vec3(0.0, 0.0, 0.0);
    up = vec3(0.0, 1.0, 0.0);

    viewMatrix = mult(viewMatrix, lookAt(eye, at, up));
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "viewMatrix"), false, flatten(viewMatrix));

    var modelViewMatrix = mult(viewMatrix, modelMatrix);
    var normalM = normalMatrix(modelViewMatrix, true);
    gl.uniformMatrix3fv(gl.getUniformLocation(program, 'normalMatrix'), false, flatten(normalM));

    gl.drawArrays(gl.TRIANGLES, 0, vertices.length);

    theta += 0.01;
    requestAnimFrame(render);

}

//helper functions to draw the 1 unit sphere
function tetrahedron(a, b, c, d, n)
{
    divideTriangle(a, b, c, n);
    divideTriangle(d, c, b, n);
    divideTriangle(a, d, b, n);
    divideTriangle(a, c, d, n);
}

function divideTriangle(a, b, c, count)
{
    if (count > 0)
    {
        var ab = normalize(mix(a, b, 0.5), true);
        var ac = normalize(mix(a, c, 0.5), true);
        var bc = normalize(mix(b, c, 0.5), true);

        divideTriangle(a, ab, ac, count - 1);
        divideTriangle(ab, b, bc, count - 1);
        divideTriangle(bc, c, ac, count - 1);
        divideTriangle(ab, bc, ac, count - 1);
    }
    else
    {
        triangle(a, b, c);
    }
}

function triangle(a, b, c)
{
    vertices.push(a);
    vertices.push(b);
    vertices.push(c);

    normals.push(a[0], a[1], a[2], 0.0);
    normals.push(b[0], b[1], b[2], 0.0);
    normals.push(c[0],c[1], c[2], 0.0);
}

//function to set the buttons' functionalities
function setButtons() {
    //1. button to increase the number of subdivisions
    document.getElementById("plus_button").onclick = function () {
        numTimesToSubdivide += 1;
        vertices = [];
        normals = [];

        tetrahedron(va, vb, vc, vd, numTimesToSubdivide);

        gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

        gl.bindBuffer(gl.ARRAY_BUFFER, nBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW);

        updateNumText();

    };

    //2. button to decrease the number of subdivisions
    document.getElementById("minus_button").onclick = function () {
        numTimesToSubdivide -= 1;
        vertices = [];
        normals = [];

        tetrahedron(va, vb, vc, vd, numTimesToSubdivide);

        gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

        gl.bindBuffer(gl.ARRAY_BUFFER, nBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW);

        updateNumText();
    };
}

function updateNumText() {
    document.getElementById("num_subdivisions").textContent = numTimesToSubdivide;
}



