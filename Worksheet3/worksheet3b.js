window.onload = init;

//global variable gl
var gl, program;

var colors = [
    vec4(0.0, 0.0, 0.0, 1.0),  // black 0
    vec4(1.0, 0.0, 0.0, 1.0),  // red 1
    vec4(1.0, 1.0, 0.0, 1.0),  // yellow 2
    vec4(0.0, 1.0, 0.0, 1.0),  // green 3
    vec4(0.0, 0.0, 1.0, 1.0),  // blue 4
    vec4(1.0, 0.0, 1.0, 1.0),  // magenta 5
    vec4(0.0, 1.0, 1.0, 1.0),  // cyan 6
    vec4(1.0, 1.0, 1.0, 1.0)   // white 7
];

var vertices = [
        vec4(1.0, 0.0, 1.0, 1.0),
        vec4(1.0, 0.0, 0.0, 1.0),
        vec4(0.0, 0.0, 1.0, 1.0),
        vec4(0.0, 0.0, 0.0, 1.0),
        vec4(1.0, 1.0, 1.0, 1.0),
        vec4(1.0, 1.0, 0.0, 1.0),
        vec4(0.0, 1.0, 1.0, 1.0),
        vec4(0.0, 1.0, 0.0, 1.0)
];


var indices = [
    1, 0,
    0, 2,
    2, 3,
    3, 1,
    4, 0,
    5, 1,
    7, 3,
    6, 2,
    5, 4,
    4, 6,
    6, 7,
    7, 5
];

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    gl.enable(gl.DEPTH_TEST);

    //Load shaders
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Array element buffer
    var iBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint8Array(indices), gl.STATIC_DRAW);

    //Color array attribute buffer
    var cBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW);

    var vColor = gl.getAttribLocation(program, "vColor");
    gl.vertexAttribPointer(vColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vColor);

    //Vertex array attribute buffer
    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Set the projection matrix (common to all the three wireframe cubes in the sphere)
    var projectionMatrix = mat4();
    var fovy = 45; //setting a field of view to 45�
    var aspectRatio = 1; //as we are displaying the cubes on a 512 x 512 viewport
    var near = 0.01;
    var far = 5;
    projectionMatrix = mult(projectionMatrix, perspective(fovy, aspectRatio, near, far));
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    //Set the view matrix (common to all the three wireframe cubes in the sphere)
    var viewMatrix = mat4();
    eye = vec3(0, 0, 3);
    at = vec3(0.0, 0.0, 0.0);
    up = vec3(0.0, 1.0, 0.0);
    viewMatrix = mult(viewMatrix, lookAt(eye, at, up));
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "viewMatrix"), false, flatten(viewMatrix));

    render();
}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    var modelMatrix = mat4(); //the modelMatrix will be set separately for each object

    //Three-point-perspective (model matrix)
    var modelMatrix = mat4();
    modelMatrix = mult(modelMatrix, scalem(0.6, 0.6, 0.6));
    //Rotate the cube both around both the X and Y axes so to achieve a proper 3 point perspective
    modelMatrix = mult(modelMatrix, rotateX(-35.26)); 
    modelMatrix = mult(modelMatrix, rotateY(45));
    modelMatrix = mult(modelMatrix, translate(-0.5, -0.5, -0.5));
    //computing the overall modelViewMatrix for the cube
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelMatrix"), false, flatten(modelMatrix));
    gl.drawElements(gl.LINES, indices.length, gl.UNSIGNED_BYTE, 0);

    //One-point-perspective (model matrix)
    modelMatrix = mat4();
    modelMatrix = mult(modelMatrix, scalem(0.6, 0.6, 0.6));
    //The cube is not rotated so to achieve a proper 1 point perspective
    modelMatrix = mult(modelMatrix, translate(-1.1, -1.1, 0));
    modelMatrix = mult(modelMatrix, translate(-0.5, -0.5, -0.5));
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelMatrix"), false, flatten(modelMatrix));
    gl.drawElements(gl.LINES, indices.length, gl.UNSIGNED_BYTE, 0);

    //Two-point-perspective (model matrix)
    modelMatrix = mat4();
    modelMatrix = mult(modelMatrix, scalem(0.52, 0.52, 0.52));
    //Rotate the cube around the Y axis so to achieve a proper 2 point perspective
    modelMatrix = mult(modelMatrix, rotateY(45));
    modelMatrix = mult(modelMatrix, translate(1.5, 1.15, 0));
    modelMatrix = mult(modelMatrix, translate(-0.5, -0.5, -0.5));
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelMatrix"), false, flatten(modelMatrix));
    gl.drawElements(gl.LINES, indices.length, gl.UNSIGNED_BYTE, 0);

}


