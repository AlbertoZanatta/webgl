// JavaScript source code
window.onload = function () {
    setButtons();
    init();
}
//global variable gl and canvas
var gl = null;
var canvas = null;

//Explicit shader variables
var shadowVertexShader = getShaderSource("shadow-vertex-shader");
var shadowFragmentShader = getShaderSource("shadow-fragment-shader");

var teapotVertexShader = getShaderSource("teapot-vertex-shader");
var teapotFragmentShader = getShaderSource("teapot-fragment-shader");

var groundVertexShader = getShaderSource("ground-vertex-shader");
var groundFragmentShader = getShaderSource("ground-fragment-shader");

//Global variables for the three programs
var shadowProgram;
var teapotProgram;
var groundPorgram;

//Variables related to the loading of the OBJ model and the objects incapsulating the data 
//for the drawing of the ground and the teapot
var g_objDoc = null;
var g_drawingInfo = null;
var teapotObject = null;
var groundObject = null;

//Custom framebuffer object
var fbo = null;

//Variables for the rotation of the light source, the translation of the teapot and the change between 'normal' and 'debug' view
var translation = 0; //for the up and down movement of the teapot

var lightPosition; //position of the light source
var theta = 0; //for the circular movement of the light source

var moveLight = false;
var moveTeapot = false;
var debugView = false;

//Global variable for checking if the ground texture has been loaded or not
var imageLoaded = false;

//Horizontal and vertical resolution (in pixel) of the shadow map
var OFFSCREEN_WIDTH = 4096, OFFSCREEN_HEIGHT = 4096;


function init()
{
    canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }

    //Enabling depth testing
    gl.enable(gl.DEPTH_TEST);
    
    //Load shaders and create the three different programs that will be used in the render function
    shadowProgram = createProgram(gl, shadowVertexShader, shadowFragmentShader);
    teapotProgram = createProgram(gl, teapotVertexShader, teapotFragmentShader);
    groundProgram = createProgram(gl, groundVertexShader, groundFragmentShader);
    
    //Create a 'custom' framebuffer object
    fbo = initFramebufferObject(gl);
    if (!fbo) {
        console.log('Failed to initialize frame buffer object');
        return;
    }

    // Set the first texture unit as active
    gl.activeTexture(gl.TEXTURE0);
    // Bind it as the texture of the 'custom' framebuffer object
    gl.bindTexture(gl.TEXTURE_2D, fbo.texture);

    //1. Create and initialize the 'teapotObject'
    teapotObject = new Object();
    
    // Start reading the OBJ file
    readOBJFile('teapot.obj', gl, teapotObject, 1, false);

    //Material properties and light source properties
    var materialAmbient = vec4(0.2, 0.2, 0.2, 1.0);
    var materialDiffuse = vec4(0.5, 0.5, 0.5, 1.0);
    var materialSpecular = vec4(0.4, 0.4, 0.4, 1.0);
    var lightEmission = vec4(0.8, 0.8, 0.8, 1.0);

    ambientProduct = mult(lightEmission, materialAmbient);
    diffuseProduct = mult(lightEmission, materialDiffuse);
    specularProduct = mult(lightEmission, materialSpecular);
    materialShininess = 500;

    //2. Create and initialize the 'groundObject'
    groundObject = new Object();

    //2.1 Set the vertices for the ground plane and store them in a buffer inside the 'groundObject'
    ground_vertices = [
        vec4(2.0, -1.0, -1.0, 1.0),
        vec4(2.0, -1.0, -5.0, 1.0),
        vec4(-2.0, -1.0, -1.0, 1.0),
        vec4(-2.0, -1.0, -5.0, 1.0)    
    ];
    groundObject.vertexBuffer = initArrayBufferForLaterUse(gl, flatten(ground_vertices), 4, gl.FLOAT);

    //2.2 Set the texture coordinates for the ground plane and store them in a buffer inside the 'groundObject';
    var texCoords = [
        vec2(1.0, 0.0),
        vec2(1.0, 1.0),
        vec2(0.0, 0.0),
        vec2(0.0, 1.0)
    ];
    groundObject.texCoordBuffer = initArrayBufferForLaterUse(gl, flatten(texCoords), 2, gl.FLOAT);
    
    //2.3 Create the texture object and retrieve (and bind) the image for the ground
    var texture1 = gl.createTexture();
    //Create an image object
    var image = new Image();
    image.crossorigin = 'anonymous';
    //Register an event handler to be called when the image loading completes
    image.onload = function () {
        //Flip the image's y axis
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        //Enable the texture unit 1
        gl.activeTexture(gl.TEXTURE1);
        //Bind the texture object to the target
        gl.bindTexture(gl.TEXTURE_2D, texture1);
        //Set the texture image
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        //Set the texture parameters
        //1. Set the wrap mode to repeat (for both s and t coordinates)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

        gl.generateMipmap(gl.TEXTURE_2D);

        //2. Set the filter modes
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);
        imageLoaded = true;
    };

    //Tell the browser to load an image
    image.src = 'xamp23.png';
   
    render();

    //3 Calculate all the matrices that will remain the same throughout the program and the calls to the render() function,
    //so that it's not necessary to re-create them every time the render function is called. They are global variables for the program.

    //3.1 Set the projection matrix
    projectionMatrix = mat4();
    var fovy = 90;
    var aspect = 1;
    var near = 0.001;
    var far = 100;
    var perspectiveMatrix = perspective(fovy, aspect, near, far);
    projectionMatrix = mult(projectionMatrix, perspectiveMatrix);

    //3.2 Set the 'normal' view matrix
    normalViewMatrix = mat4();

    //3.3 Set the 'debug' view matrix
    debugViewMatrix = mat4();
    eye = vec3(0.0, 3, -2.9);
    at = vec3(0.0, -1.0, -3);
    up = vec3(0.0, 1.0, 0.0);
    debugViewMatrix = mult(debugViewMatrix, lookAt(eye, at, up));

    //3.4 The lightProjectionMatrix ( projection matrix used to draw the shadowmap, so when rendering the scene from the light's point of view)
    lightProjectionMatrix = mat4();
    var fovy = 130;
    var aspect = OFFSCREEN_WIDTH / OFFSCREEN_HEIGHT;
    var near = 1;
    var far = 5;
    var perspectiveMatrix = perspective(fovy, aspect, near, far);
    lightProjectionMatrix = mult(lightProjectionMatrix, perspectiveMatrix);
}

function render()
{
    if (!g_drawingInfo && g_objDoc && g_objDoc.isMTLComplete())
    {
        // OBJ and all MTLs are available
        g_drawingInfo = onReadComplete(gl, teapotObject, g_objDoc);  
    }
    if (!g_drawingInfo || !imageLoaded) //either the teapot or the ground texture have not been initialized
    {
        requestAnimFrame(render);
        return;
    }
    
    //Calculate the updated light position
    lightPosition = vec3(Math.cos(theta), 2.0, -3 + Math.sin(theta)); //The light moves around a circle or radius = 1 around point (0.0, 2.0, -3.0)
    
    var viewMatrix = normalViewMatrix;
    //If the user has selected the 'debug view' mode, the debugView matrix becomes the default view matrix for the drawing of the objects in the scene
    if (debugView) {
        viewMatrix = debugViewMatrix;
    }
    
    //A Calculate the view matrix that will be used both to draw the ground and the teapot from the light's source point of view
    var lightViewMatrix = mat4();
    eye = lightPosition;
    at = vec3(0.0, 0.0, -3); //point lying at the center of the ground plane
    up = vec3(0.0, 1.0, 0.0); //usual 'up' direction
    var lookAtMatrix = lookAt(eye, at, up);
    lightViewMatrix = mult(lightViewMatrix, lookAtMatrix);

    //B. Draw the shadow map
    drawShadowMap(shadowProgram, lightViewMatrix, lightProjectionMatrix);

    //C. Drawing the scene
    // Change the drawing destination to the default framebuffer
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    // Set the view port for the default framebuffer (height and width correspond to that of the canvas object) 
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    // Clear color and depth buffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //C.1 Draw the ground
    drawGround(groundProgram, viewMatrix, projectionMatrix, mult(lightProjectionMatrix, lightViewMatrix));
    //C.2 Draw the teapot
    drawTeapot(teapotProgram, viewMatrix, projectionMatrix, mult(lightProjectionMatrix, lightViewMatrix));


    //Moving the light (if the option has  been selected)
    if (moveTeapot)
    {
        translation += 0.01;
    }

    //Moving the teapot (if the option has been selected)
    if (moveLight)
    {
        theta += 0.01;
    }

    if (moveTeapot || moveLight)
    {
        requestAnimationFrame(render);
    }     
}

function drawShadowMap(shadowProgram, lightViewMatrix, lightProjectionMatrix) {
    //1. Generate the shadow map
    //1.1 Bind the custom framebuffer object and set the main drawing parameters
    //Set the drawing destination to the custom framebuffer object  
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    // Set the view port for the FBO (height abd width correspond to the desired resolution for the shadow map)
    gl.viewport(0, 0, OFFSCREEN_WIDTH, OFFSCREEN_HEIGHT);
    // Clear the color and depth buffers of the custom framebuffer 
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //Enable depth testing
    gl.enable(gl.DEPTH_TEST);
    //Set the right shaders for drawing the shadow map
    gl.useProgram(shadowProgram);

    //2. Draw the ground in the shadow map
    //2.2 Bind the vertex positions of the ground object
    initAttributeVariable(gl, gl.getAttribLocation(shadowProgram, 'vPosition'), groundObject.vertexBuffer);
    //2.2 Set and bind the Model, View and Projection matrices
    var groundLightModelMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgram, 'modelMatrix'), false, flatten(groundLightModelMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgram, 'viewMatrix'), false, flatten(lightViewMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgram, 'projectionMatrix'), false, flatten(lightProjectionMatrix));
    //2.3 Draw the ground object
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    //3. Draw the teapot in the shadow map
    //3.1 Bind the vertex positions and the array of vertex indices
    initAttributeVariable(gl, gl.getAttribLocation(shadowProgram, 'vPosition'), teapotObject.vertexBuffer);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teapotObject.indexBuffer);
    //3.2 Set the correct model matrix for the teapot (it is resized and translated in the scene)
    var teapotLightModelMatrix = mat4();
    teapotLightModelMatrix = mult(teapotLightModelMatrix, translate(0, -1, -3));
    teapotLightModelMatrix = mult(teapotLightModelMatrix, translate(0, triangularWave(translation), 0));
    teapotLightModelMatrix = mult(teapotLightModelMatrix, scalem(0.25, 0.25, 0.25));
    gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgram, 'modelMatrix'), false, flatten(teapotLightModelMatrix));
    //A3.3 Draw the teapot object
    gl.drawElements(gl.TRIANGLES, g_drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0);
}

function drawTeapot(teapotProgram, viewMatrix, projectionMatrix, VPLightMatrix) {
    //1.Draw the teapot
    //1.1 Set the shaders for drawing the teapot
    gl.useProgram(teapotProgram);
    //1.2 Bind the vertex and normal buffers to the corresponding attributes in the vertex shader and enable them
    initAttributeVariable(gl, gl.getAttribLocation(teapotProgram, 'vPosition'), teapotObject.vertexBuffer);
    initAttributeVariable(gl, gl.getAttribLocation(teapotProgram, 'vNormal'), teapotObject.normalBuffer);
    //Bind the array of vertex indices
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teapotObject.indexBuffer);

    //1.3 Bind the Model, View, Projection and Normal  matrices
    var teapotModelMatrix = mat4();
    teapotModelMatrix = mult(teapotModelMatrix, translate(0, -1, -3));
    teapotModelMatrix = mult(teapotModelMatrix, translate(0, triangularWave(translation), 0));
    teapotModelMatrix = mult(teapotModelMatrix, scalem(0.25, 0.25, 0.25));
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, 'modelMatrix'), false, flatten(teapotModelMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, 'viewMatrix'), false, flatten(viewMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, 'projectionMatrix'), false, flatten(projectionMatrix));
    var modelViewMatrix = mult(viewMatrix, teapotModelMatrix);
    var normalM = normalMatrix(modelViewMatrix, true);
    gl.uniformMatrix3fv(gl.getUniformLocation(teapotProgram, 'normalMatrix'), false, flatten(normalM));

    //1.4 Bind all the information useful to calculate the lighting
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "lightPosition"), flatten(vec4(lightPosition, 1.0)));
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "ambientProduct"), flatten(ambientProduct));
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "diffuseProduct"), flatten(diffuseProduct));
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "specularProduct"), flatten(specularProduct));
    gl.uniform1f(gl.getUniformLocation(teapotProgram, "shininess"), materialShininess);

    //Use the shadow map for self-shadowing on the teapot
    var TeapotMvpMatrixFromLight = mat4();
    TeapotMvpMatrixFromLight = mult(VPLightMatrix, teapotModelMatrix);
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, 'MvpMatrixFromLight'), false, flatten(TeapotMvpMatrixFromLight));
    gl.uniform1i(gl.getUniformLocation(teapotProgram, "shadowMap"), 0);

    //1.5 Draw the teapot object
    gl.drawElements(gl.TRIANGLES, g_drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0);
}

function drawGround(groundProgram, viewMatrix, projectionMatrix, VPLightMatrix) {
    //1. Draw the ground
    //1.1 Set the shaders for drawing the ground
    gl.useProgram(groundProgram);

    //1.2 Bind the vertex and texture coordinates buffers to the corresponding attributes in the vertex shader and enable them
    initAttributeVariable(gl, gl.getAttribLocation(groundProgram, 'vPosition'), groundObject.vertexBuffer);
    initAttributeVariable(gl, gl.getAttribLocation(groundProgram, 'vTexCoord'), groundObject.texCoordBuffer);

    //1.3 Bind the Model, View and Projection matrices
    var groundModelMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(groundProgram, 'modelMatrix'), false, flatten(groundModelMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(groundProgram, 'viewMatrix'), false, flatten(viewMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(groundProgram, 'projectionMatrix'), false, flatten(projectionMatrix));

    //1.4 Bind the Model-View-Projection matrix related to the light's point of view
    var GroundMvpMatrixFromLight = mat4();
    MvpMatrixFromLight = mult(VPLightMatrix, groundModelMatrix);
    gl.uniformMatrix4fv(gl.getUniformLocation(groundProgram, 'MvpMatrixFromLight'), false, flatten(MvpMatrixFromLight));

    //1.5 Bind the shadow map texture and the proper ground texture as the corresponding uniform samplers in the groundProgram fragment shader
    gl.uniform1i(gl.getUniformLocation(groundProgram, "shadowMap"), 0);
    gl.uniform1i(gl.getUniformLocation(groundProgram, "texMap"), 1);

    //1.6 Draw the ground object
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}


function initArrayBufferForLaterUse(gl, data, num, type) {
    // Create a buffer object
    var buffer = gl.createBuffer();
    if (!buffer) {
        console.log('Failed to create the buffer object');
        return null;
    }
    // Write date into the buffer object
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

    // Store the necessary information to assign the object to the attribute variable later
    buffer.num = num;
    buffer.type = type;

    return buffer;
}

function initElementArrayBufferForLaterUse(gl, data, type) {
    // Create a buffer object
    var buffer = gl.createBuffer();
    if (!buffer) {
        console.log('Failed to create the buffer object');
        return null;
    }
    // Write date into the buffer object
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, data, gl.STATIC_DRAW);

    buffer.type = type;

    return buffer;
}

function initAttributeVariable(gl, a_attribute, buffer) {
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.vertexAttribPointer(a_attribute, buffer.num, buffer.type, false, 0, 0);
    gl.enableVertexAttribArray(a_attribute);
}


//Read a file
function readOBJFile(fileName, gl, model, scale, reverse) 
{
    var request = new XMLHttpRequest();
   
    request.onreadystatechange = function() 
    {
        if (request.readyState === 4 && request.status !== 404) 
        {
            onReadOBJFile(request.responseText, fileName, gl, model, scale, reverse);
         }
    }
    request.open('GET', fileName, true); // Create a request to get file
    request.send(); // Send the request
}



// OBJ file has been read
function onReadOBJFile(fileString, fileName, gl, o, scale, reverse) 
{
    var objDoc = new OBJDoc(fileName); // Create a OBJDoc object
    var result = objDoc.parse(fileString, scale, reverse);
    if (!result) 
    {
        g_objDoc = null; g_drawingInfo = null;
        console.log("OBJ file parsing error.");
        return;
    }
    g_objDoc = objDoc;
}

// OBJ File has been read completely
function onReadComplete(gl, model, objDoc)
{
    // Acquire the vertex coordinates and colors from OBJ file
    var drawingInfo = objDoc.getDrawingInfo();

    // Write data and indices into the buffer objects
    model.vertexBuffer = initArrayBufferForLaterUse(gl, drawingInfo.vertices, 3, gl.FLOAT);
    model.normalBuffer = initArrayBufferForLaterUse(gl, drawingInfo.normals, 3, gl.FLOAT);
    model.indexBuffer = initElementArrayBufferForLaterUse(gl, drawingInfo.indices, gl.UNSIGNED_BYTE);
    model.numIndices = drawingInfo.indices.length;
    return drawingInfo;
}

function getShaderSource(vertexShaderId)
{
    var vertElem = document.getElementById( vertexShaderId );
    if ( !vertElem ) { 
        alert( "Unable to load vertex shader " + vertexShaderId );
        return -1;
    }
    else {
        return vertElem.text;
    }

}

function initFramebufferObject(gl) {
    var framebuffer, texture, depthBuffer;

    // Define the error handling function
    var error = function () {
        if (framebuffer) gl.deleteFramebuffer(framebuffer);
        if (texture) gl.deleteTexture(texture);
        if (depthBuffer) gl.deleteRenderbuffer(depthBuffer);
        return null;
    }

    // Create a framebuffer object (FBO)
    framebuffer = gl.createFramebuffer();
    if (!framebuffer) {
        console.log('Failed to create frame buffer object');
        return error();
    }

    // Create a texture object and set its size and parameters
    texture = gl.createTexture(); // Create a texture object
    if (!texture) {
        console.log('Failed to create texture object');
        return error();
    }
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, OFFSCREEN_WIDTH, OFFSCREEN_HEIGHT, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

    // Create a renderbuffer object and Set its size and parameters
    depthBuffer = gl.createRenderbuffer(); // Create a renderbuffer object
    if (!depthBuffer) {
        console.log('Failed to create renderbuffer object');
        return error();
    }
    gl.bindRenderbuffer(gl.RENDERBUFFER, depthBuffer);
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, OFFSCREEN_WIDTH, OFFSCREEN_HEIGHT);

    // Attach the texture and the renderbuffer object to the FBO
    gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, depthBuffer);

    // Check if FBO is configured correctly
    var e = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
    if (gl.FRAMEBUFFER_COMPLETE !== e) {
        console.log('Frame buffer object is incomplete: ' + e.toString());
        return error();
    }

    framebuffer.texture = texture; // keep the required object

    // Unbind the buffer object
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);

    return framebuffer;
}

//Return the value for translation the teapot up and down
function triangularWave(x) {
    return (((2.0 / Math.PI) * Math.asin(Math.sin(Math.PI * x)))) + 1;
}

//Functions to initialize the buttons in the scene
function setButtons() {
    //1. Button for moving the light source
    document.getElementById("light_button").onclick = function () {
        moveLight = !moveLight;
        if (moveLight) {
            document.getElementById("light_button").innerHTML = "Stop light";
            if(!moveTeapot)
            {
                requestAnimationFrame(render);
            }
        }
        else {
            document.getElementById("light_button").innerHTML = "Move light";
        }
    };

    //2. Button for moving the teapot object
    document.getElementById("teapot_button").onclick = function () {
        moveTeapot = !moveTeapot;
        if (moveTeapot) {
            document.getElementById("teapot_button").innerHTML = "Stop teapot";
            if (!moveLight) {
                requestAnimationFrame(render);
            }
        }
        else {
            document.getElementById("teapot_button").innerHTML = "Move teapot";
        }
    };

    //3. Button for changing between 'normal' and 'debug' views
    document.getElementById("debug_button").onclick = function () {
        debugView = !debugView;
        if (debugView) {
            document.getElementById("debug_button").innerHTML = "Normal view";
        }
        else {
            document.getElementById("debug_button").innerHTML = "Debug view";
        }

        if(!moveLight && !moveTeapot)
            requestAnimationFrame(render);
    };

}

