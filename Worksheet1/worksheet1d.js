window.onload = init;

var vertices = [];

//Square angle of rotation
var theta = 0;

function init() {
    canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    // Load shaders 
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    vertices =
     [
         vec4(0.5, -0.5, 0, 1),
         vec4(0.5, 0.5, 0, 1),
         vec4(-0.5, -0.5, 0, 1),
         vec4(-0.5, 0.5, 0, 1)
     ];

    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT);
    var rotationMatrix = rotateZ(theta);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "rotationMatrix"), false, flatten(rotationMatrix));
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, vertices.length);
    theta += 0.1;
    requestAnimationFrame(render);
}
