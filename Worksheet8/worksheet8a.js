// JavaScript source code
window.onload = function () {
    setButtons();
    init();
}
//Global variable gl 
var gl = null;

//Explicit shaders variables
var teapotVertexShader = getShaderSource("teapot-vertex-shader");
var teapotFragmentShader = getShaderSource("teapot-fragment-shader");

var groundVertexShader = getShaderSource("ground-vertex-shader");
var groundFragmentShader = getShaderSource("ground-fragment-shader");

//Global variables for the two programs
var teapotProgram = null;
var groundProgram = null;

//Variables related to the loading of the OBJ model and the objects incapsulating the data 
//for the drawing of the ground and the teapot
var g_objDoc = null; // The information of OBJ file
var g_drawingInfo = null; // The information for drawing 3D model
var teapotObject = null;
var groundObject = null;

//Angle for the rotating camera (theta) and value for translating the teapot (translation)
var theta = 0;
var translation = 0;
var light; //position of the light source

//Boolean values for managing the movement of the light/teapot and the view (allowing to select between 'normal' view and 'debug' view).
var moveLight = false;
var moveTeapot = false;
var debugView = false;

//Global boolean variable set to true when the ground texture is fully loaded
var imageLoaded = false;

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }

    //Enable depth testing
    gl.enable(gl.DEPTH_TEST);

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    
    //Create the teapot and the ground programs from the shaders compiled outside the init() function
    teapotProgram = createProgram(gl, teapotVertexShader, teapotFragmentShader);
    groundProgram = createProgram(gl, groundVertexShader, groundFragmentShader);
 
    
    //1. Create and initialize the 'teapot object'
    teapotObject = new Object();
    
    //Start reading the OBJ file
    readOBJFile('teapot.obj', gl, teapotObject, 1, false);

    //Material properties and light source properties
    var materialAmbient = vec4(0.2, 0.2, 0.2, 1.0);
    var materialDiffuse = vec4(0.5, 0.5, 0.5, 1.0);
    var materialSpecular = vec4(0.4, 0.4, 0.4, 1.0);
    var lightEmission = vec4(0.8, 0.8, 0.8, 1.0);

    ambientProduct = mult(lightEmission, materialAmbient);
    diffuseProduct = mult(lightEmission, materialDiffuse);
    specularProduct = mult(lightEmission, materialSpecular);
    materialShininess = 500;
 
    //2. Create and initialize the 'ground object'
    groundObject = new Object();

    //2.1 Set the vertices for the ground plane and store them in a buffer inside the 'groundObject'
    plane_vertices = [
        vec4(2.0, -1.0, -1.0, 1.0),
        vec4(2.0, -1.0, -5.0, 1.0),
        vec4(-2.0, -1.0, -1.0, 1.0),
        vec4(-2.0, -1.0, -5.0, 1.0)    
    ];
    groundObject.vertexBuffer = initArrayBufferForLaterUse(gl, flatten(plane_vertices), 4, gl.FLOAT);

    //2.2 Set the texture coordinates for the ground plane and store them in a buffer inside the 'groundObject';
    var texCoords = [
        vec2(0.0, 0.0),
        vec2(0.0, 1.0),
        vec2(1.0, 0.0),
        vec2(1.0, 1.0)
    ];
    groundObject.texCoordBuffer = initArrayBufferForLaterUse(gl, flatten(texCoords), 2, gl.FLOAT);

    //2.3 Create the texture object and retrieve (and bind) the image for the ground
    var texture0 = gl.createTexture();
    //Create an image object
    var image = new Image();
    image.crossorigin = 'anonymous';
    //Register an event handler to be called when the image loading completes
    image.onload = function () {
        //Flip the image's y axis
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        //Enable the texture unit 0
        gl.activeTexture(gl.TEXTURE0);
        //Bind the texture object to the target
        gl.bindTexture(gl.TEXTURE_2D, texture0);
        //Set the texture image
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        //Set the texture parameters
        //1. Set the wrap mode to repeat (for both s and t coordinates)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

        gl.generateMipmap(gl.TEXTURE_2D);

        //2. Set the filter modes
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);
        imageLoaded = true;
    };

    //Tell the browser to load an image
    image.src = 'xamp23.png';

    //3 Calculate all the matrices that will remain the same throughout the program and the calls to the render() function,
    //so that it's not necessary to re-create them every time the render function is called. They are global variables for the program.

    //3.1 Matrix for the shadow projection
    light = vec3(Math.cos(theta), 2.0, -3 + Math.sin(theta));
    //Set the shadow - projection matrix initially as an identity matrix
    M = mat4();
    M[3][3] = 0;
    //Subtract the y coordinate of the plane y = -1 - and therefore add 1 - to the y coordinate of the light position 
    //(which will remain the same even if the light is moving on a circle) to achieve a projection onto such plane;
    M[3][1] = -1.0 / (light[1] + 1 + 0.005); 
                                           
    //3.2 Set the projection matrix
    projectionMatrix = mat4();
    var fovy = 90;
    var aspect = 1;
    var near = 0.001;
    var far = 100;
    var perspectiveMatrix = perspective(fovy, aspect, near, far);
    projectionMatrix = mult(projectionMatrix, perspectiveMatrix);

    //3.3 Set the 'normal' view matrix
    normalViewMatrix = mat4();

    //3.4 Set the 'debug' view matrix
    debugViewMatrix = mat4();
    eye = vec3(0.0, 3, -2.9); 
    at = vec3(0.0, -1.0, -3);  
    up = vec3(0.0, 1.0, 0.0);
    debugViewMatrix = mult(debugViewMatrix, lookAt(eye, at, up));

    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    if (!g_drawingInfo && g_objDoc && g_objDoc.isMTLComplete())
    {
        // OBJ and all MTLs are available
        g_drawingInfo = onReadComplete(gl, teapotObject, g_objDoc);  
    }
    if (!g_drawingInfo || !imageLoaded) //either the teapot object or the ground texture are still not loaded 
    {
        requestAnimFrame(render);
        return;
    }

    //Calculate the updated light position
    light = vec3(Math.cos(theta), 2.0, -3 + Math.sin(theta));

    var viewMatrix = normalViewMatrix;
    //If the user has selected the 'debug view' mode, the debugView matrix becomes the default view matrix for the drawing of the objects in the scene
    if (debugView)
    {
        viewMatrix = debugViewMatrix;
    }

    //1. Draw the ground object
    drawGround(groundProgram, viewMatrix, projectionMatrix);

    //2. Draw the teapot shadow and the teapot itself
    drawShadowAndTeapot(teapotProgram, viewMatrix, projectionMatrix);
       
    //Finally, if the user has selected the option to move the teapot up and down (moveTeapot = true) or the option to move the
    // light source (moveLight = true) I slightly increment either the translation or the theta variables so that in the next frame
    // either the teapot or the light will be in a different position
    if (moveTeapot)
    {
        translation += 0.005;
    }

    if (moveLight)
    {
        theta += 0.01;
    }

    if (moveTeapot || moveLight)
        requestAnimationFrame(render);
}

function drawShadowAndTeapot(teapotProgram, viewMatrix, projectionMatrix) {
    //1. Initalize correctly most of the uniform and attribute variables
    //1.1 Set the shaders for drawing the shadow/teapot
    gl.useProgram(teapotProgram);
    //1.2 Bind the vertex and normal buffers to the corresponding attributes in the vertex shader and enable them
    initAttributeVariable(gl, gl.getAttribLocation(teapotProgram, 'vPosition'), teapotObject.vertexBuffer);
    initAttributeVariable(gl, gl.getAttribLocation(teapotProgram, 'vNormal'), teapotObject.normalBuffer);
    //Bind the array of vertex indices
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teapotObject.indexBuffer);

    //1.3 Bind the Model, View, Projection and Normal matrices
    var teapotModelMatrix = mat4();
    teapotModelMatrix = mult(teapotModelMatrix, translate(0, -1, -3));
    //the teapot is move slightly upward/downward according to the current 'translation' value - this is used to move the teapot up and down in the scene
    teapotModelMatrix = mult(teapotModelMatrix, translate(0, triangularWave(translation), 0)); 
    teapotModelMatrix = mult(teapotModelMatrix, scalem(0.25, 0.25, 0.25));
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, 'modelMatrix'), false, flatten(teapotModelMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, 'viewMatrix'), false, flatten(viewMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, 'projectionMatrix'), false, flatten(projectionMatrix));
    var modelViewMatrix = mult(viewMatrix, teapotModelMatrix);
    var normalM = normalMatrix(modelViewMatrix, true);
    gl.uniformMatrix3fv(gl.getUniformLocation(teapotProgram, 'normalMatrix'), false, flatten(normalM));
   
    //1.4 Bind all the information useful to calculate the lighting
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "lightPosition"), flatten(vec4(light, 1.0)));
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "ambientProduct"), flatten(ambientProduct));
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "diffuseProduct"), flatten(diffuseProduct));
    gl.uniform4fv(gl.getUniformLocation(teapotProgram, "specularProduct"), flatten(specularProduct));
    gl.uniform1f(gl.getUniformLocation(teapotProgram, "shininess"), materialShininess);

    //2. Draw the teapot's shadow
    //2.1 Set the right depth function
    gl.depthFunc(gl.GREATER);
    //2.2 Set and bind the correct shadow matrix
    var shadowMatrix = mat4();
    shadowMatrix = mult(shadowMatrix, translate(light[0], light[1], light[2])); //translate everything back to previous woorld coordinates
    shadowMatrix = mult(shadowMatrix, M); //projection of the vertices onto the plane y = -1
    shadowMatrix = mult(shadowMatrix, translate(-light[0], -light[1], -light[2])); //translation to get the light source position at the center of the world coordinates
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, "shadowMatrix"), false, flatten(shadowMatrix));
    //2.3 Set the uniform 'visibility' variable in the fragment shader to 0.0
    gl.uniform1f(gl.getUniformLocation(teapotProgram, "visibility"), 0.0);
    //2.4 Draw the shadow
    gl.drawElements(gl.TRIANGLES, g_drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0);

    //3 Draw the actual teapot
    //3.1 Restore the standard depth function
    gl.depthFunc(gl.LESS);
    //3.2 Set the shadow matrix to the identity
    var shadowMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(teapotProgram, "shadowMatrix"), false, flatten(shadowMatrix));
    //3.3 Set the uniform 'visibility' variable in the fragment shader to 1.0
    gl.uniform1f(gl.getUniformLocation(teapotProgram, "visibility"), 1.0);
    //2.4 Draw the teapot
    gl.drawElements(gl.TRIANGLES, g_drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0);
}

function drawGround(groundProgram, viewMatrix, projectionMatrix) {
    //1. Draw the ground
    //1.1 Set the shaders for drawing the ground
    gl.useProgram(groundProgram);

    //1.2 Bind the vertex and texture coordinates buffers to the corresponding attributes in the vertex shader and enable them
    initAttributeVariable(gl, gl.getAttribLocation(groundProgram, 'vPosition'), groundObject.vertexBuffer);
    initAttributeVariable(gl, gl.getAttribLocation(groundProgram, 'vTexCoord'), groundObject.texCoordBuffer);

    //1.3 Bind the Model, View and Projection matrices
    var groundModelMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(groundProgram, 'modelMatrix'), false, flatten(groundModelMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(groundProgram, 'viewMatrix'), false, flatten(viewMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(groundProgram, 'projectionMatrix'), false, flatten(projectionMatrix));

    //1.4 Bind the shadow map texture and the proper ground texture as the corresponding uniform samplers in the groundProgram fragment shader
    gl.uniform1i(gl.getUniformLocation(groundProgram, "texMap"), 0);

    //1.5 Draw the ground object
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}



function initArrayBufferForLaterUse(gl, data, num, type){
    var buffer = gl.createBuffer(); // Create a buffer object
    // Write data to the buffer object
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
    // Store information to assign it to attribute variable later
    buffer.num = num;
    buffer.type = type;
   return buffer;
}

function initElementArrayBufferForLaterUse(gl, data, type) {
    // Create a buffer object
    var buffer = gl.createBuffer();
    if (!buffer) {
        console.log('Failed to create the buffer object');
        return null;
    }
    // Write date into the buffer object
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, data, gl.STATIC_DRAW);

    buffer.type = type;

    return buffer;
}

function initAttributeVariable(gl, a_attribute, buffer) {
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.vertexAttribPointer(a_attribute, buffer.num, buffer.type, false, 0, 0);
    gl.enableVertexAttribArray(a_attribute);
}

function createEmptyArrayBuffer(gl, a_attribute, num, type)
{
    var buffer = gl.createBuffer(); // Create a buffer object
  
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);
    gl.enableVertexAttribArray(a_attribute); // Enable the assignment
   
    return buffer;
}

//Read a file
function readOBJFile(fileName, gl, model, scale, reverse) 
{
    var request = new XMLHttpRequest();
   
    request.onreadystatechange = function() 
    {
        if (request.readyState === 4 && request.status !== 404) 
        {
            onReadOBJFile(request.responseText, fileName, gl, model, scale, reverse);
         }
    }
    request.open('GET', fileName, true); // Create a request to get file
    request.send(); // Send the request
}



// OBJ file has been read
function onReadOBJFile(fileString, fileName, gl, o, scale, reverse) 
{
    var objDoc = new OBJDoc(fileName); // Create a OBJDoc object
    var result = objDoc.parse(fileString, scale, reverse);
    if (!result) 
    {
        g_objDoc = null; g_drawingInfo = null;
        console.log("OBJ file parsing error.");
        return;
    }
    g_objDoc = objDoc;
}

// OBJ File has been read completely
function onReadComplete(gl, model, objDoc)
{
    // Acquire the vertex coordinates and colors from OBJ file
    var drawingInfo = objDoc.getDrawingInfo();

    // Write data and indices into the buffer objects
    model.vertexBuffer = initArrayBufferForLaterUse(gl, drawingInfo.vertices, 3, gl.FLOAT);
    model.normalBuffer = initArrayBufferForLaterUse(gl, drawingInfo.normals, 3, gl.FLOAT);
    model.indexBuffer = initElementArrayBufferForLaterUse(gl, drawingInfo.indices, gl.UNSIGNED_BYTE);
    model.numIndices = drawingInfo.indices.length;
    return drawingInfo;
}

function getShaderSource(vertexShaderId)
{
    var vertElem = document.getElementById( vertexShaderId );
    if ( !vertElem ) { 
        alert( "Unable to load vertex shader " + vertexShaderId );
        return -1;
    }
    else {
        return vertElem.text;
    }

}

//Return the value for translation the teapot up and down
function triangularWave(x) {
    return (((2.0 / Math.PI) * Math.asin(Math.sin(Math.PI * x)))) + 1;
}

//Functions to initialize the buttons in the scene
function setButtons()
{
    //1. Button for moving the light source
    document.getElementById("light_button").onclick = function () {
        moveLight = !moveLight;
        if (moveLight) {
            document.getElementById("light_button").innerHTML = "Stop light";
            if (!moveTeapot) {
                requestAnimationFrame(render);
            }
        }
        else {
            document.getElementById("light_button").innerHTML = "Move light";
        }

        if (!moveTeapot)
            requestAnimationFrame(render);
    };

    //2. Button for moving the teapot object
    document.getElementById("teapot_button").onclick = function () {
        moveTeapot = !moveTeapot;
        if (moveTeapot) {
            document.getElementById("teapot_button").innerHTML = "Stop teapot";
            if (!moveLight) {
                requestAnimationFrame(render);
            }
        }
        else {
            document.getElementById("teapot_button").innerHTML = "Move teapot";
        }

        if(!moveLight)
            requestAnimationFrame(render);
    };

    //3. Button for changing between 'normal' and 'debug' views
    document.getElementById("debug_button").onclick = function () {
        debugView = !debugView;
        if (debugView) {
            document.getElementById("debug_button").innerHTML = "Normal view";
        }
        else {
            document.getElementById("debug_button").innerHTML = "Debug view";
        }

        if (!moveLight && !moveTeapot)
            requestAnimationFrame(render);
    };
}

