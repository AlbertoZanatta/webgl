window.onload = init;
var maxNumVertices = 10000;
var mode = "POINT";

var trianglePointsArray = [];
var triangleColorsArray = [];

var singlePointsArray = [];
var pointsColorsArray = [];

var numPrevPoints = 0;

var colors = [
    vec4(0.3921, 0.5843, 0.9294, 1.0), // default
    vec4(0.0, 0.0, 0.0, 1.0),  // black
    vec4(1.0, 0.0, 0.0, 1.0),  // red
    vec4(1.0, 1.0, 0.0, 1.0),  // yellow
    vec4(0.0, 1.0, 0.0, 1.0),  // green
    vec4(0.0, 0.0, 1.0, 1.0),  // blue
    vec4(1.0, 0.0, 1.0, 1.0),  // magenta
    vec4(0.0, 1.0, 1.0, 1.0),  // cyan
    vec4(1.0, 1.0, 1.0, 1.0)   // white
];

var vertexColor = colors[1];
var backgroundColor = colors[0];

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    // Load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);
 
    buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, 16 * maxNumVertices, gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    cBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, 16 * maxNumVertices, gl.STATIC_DRAW);

    var vColor = gl.getAttribLocation(program, "vColor");
    gl.vertexAttribPointer(vColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vColor);

    //1 - Add points and colors to draw
    document.getElementById("canv").onmousedown = function () {
        var mouseRect = event.target.getBoundingClientRect();

        var xCanvas = event.clientX - mouseRect.left;
        var yCanvas = event.clientY - mouseRect.top;

        var xPoint = -1 + (2 * xCanvas) / canvas.width;
        var yPoint = -1 + (2 * (canvas.height - yCanvas)) / canvas.height;
        
        var vertex = vec4(xPoint, yPoint, 0.0, 1.0);
        var color= vertexColor;

        if (mode == "POINT") {
            singlePointsArray.push(vertex);
            pointsColorsArray.push(vertexColor);
        }
        else if (mode == "TRIANGLE") {
            if (numPrevPoints < 2) {
                singlePointsArray.push(vertex);
                pointsColorsArray.push(vertexColor);
                numPrevPoints++;
            }
            else {
                var vertex2 = singlePointsArray.pop();
                trianglePointsArray.push(singlePointsArray.pop());
                trianglePointsArray.push(vertex2);

                var colorVertex2 = pointsColorsArray.pop();
                triangleColorsArray.push(pointsColorsArray.pop());
                triangleColorsArray.push(colorVertex2);

                trianglePointsArray.push(vertex);
                triangleColorsArray.push(color);

                numPrevPoints = 0;
            }
        }

        var pointsArray = singlePointsArray.concat(trianglePointsArray);
        var colorsArray = pointsColorsArray.concat(triangleColorsArray);

        if (pointsArray.length <= maxNumVertices)
        {

        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(pointsArray));

        gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(colorsArray));

        }

        render();
    };

    //2. Clear Button
    var clearButton = document.getElementById("clear_button");
    clearButton.addEventListener("click", function () {

        trianglePointsArray = [];
        triangleColorsArray = [];
        singlePointsArray = [];
        pointsColorsArray = [];

        numPrevPoints = 0;

        gl.clearColor(backgroundColor[0], backgroundColor[1], backgroundColor[2], backgroundColor[3]);

        render();
    });

    //3. Select background color
    var cbm = document.getElementById("color_back_menu");
    cbm.addEventListener("click", function () {
        backgroundColor = colors[cbm.selectedIndex];
    });

    //4. Select vertex color
    var cdm = document.getElementById("color_draw_menu");
    cdm.addEventListener("click", function () {
        vertexColor = colors[cdm.selectedIndex];
    });

    //5. Point mode
    document.getElementById("point_button").onclick = function () {
        changeMode("POINT");
        numPrevPoints = 0;
    };

    //6. Triangle mode
    document.getElementById("triangle_button").onclick = function () {
        changeMode("TRIANGLE");
    };


    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT);
    if (singlePointsArray.length > 0)
    {
        gl.drawArrays(gl.POINTS, 0, singlePointsArray.length);
    }
    if (trianglePointsArray.length > 0)
    {
        gl.drawArrays(gl.TRIANGLES, singlePointsArray.length, trianglePointsArray.length);
    }  
}

function changeMode(string) {
    mode = string;
    document.getElementById("mode_span").textContent = string;
}