window.onload = init;
var maxNumVertices = 10000;
var numVertices = 0;

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);

    // Load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);
 
    buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, 16 * maxNumVertices, gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    document.getElementById("canv").onmousedown = function () {
        var mouseRect = event.target.getBoundingClientRect();

        var xCanvas = event.clientX - mouseRect.left;
        var yCanvas = event.clientY - mouseRect.top;

        var xPoint = -1 + (2 * xCanvas) / canvas.width;
        var yPoint = -1 + (2 * (canvas.height - yCanvas)) / canvas.height;

        var vertex = [vec4(xPoint, yPoint, 0.0, 1.0)];

        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.bufferSubData(gl.ARRAY_BUFFER, 16 * numVertices, flatten(vertex));

        if (numVertices < maxNumVertices)
        {
            
            numVertices += 1;
        }

        render();
    };

    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT);
    if (numVertices > 0)
    {
        gl.drawArrays(gl.POINTS, 0, numVertices);
    }
}