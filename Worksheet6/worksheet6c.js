window.onload = function () {
    setMenus();
    init();
}

//Global gl and program variables
var gl = null;
var program = null;

//Array of vertices and array of normals
var vertices = [];
var normals = [];

//Angle for the rotation of the camera around the sphere
var theta = 0;

//Arrays for the selection of the desired filter-mode
var filterModesMag = [];
var filterModesMin = [];

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl)
    {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    gl.enable(gl.DEPTH_TEST);

    //Load program shaders 
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Enable depth testing
    gl.enable(gl.DEPTH_TEST);

    //Initial values for drawing the sphere using recursive subdivision
    var numTimesToSubdivide = 6;
    var va = vec4(0.0, 0.0, 1.0, 1);
    var vb = vec4(0.0, 0.942809, -0.333333, 1);
    var vc = vec4(-0.816497, -0.471405, -0.333333, 1);
    var vd = vec4(0.816497, -0.471405, -0.333333, 1);


    //Calculate the vertices of the sphere using recursive subdivision
    tetrahedron(va, vb, vc, vd, numTimesToSubdivide);

    //Set and bind the normal array attribute buffer
    var nBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, nBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW);

    var vNormal = gl.getAttribLocation(program, "vNormal");
    gl.vertexAttribPointer(vNormal, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vNormal);

    //Set and bind the vertex array attribute buffer
    var vBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //Set light direction and components
    var lightDirection = vec4(0.0, 0.0, -1.0, 0.0);
    var lightAmbient = vec4(0.5, 0.5, 0.5, 1.0);
    var lightDiffuse = vec4(1.0, 1.0, 1.0, 1.0);

    //Bind these values to the uniform variables in the shaders' program
    gl.uniform4fv(gl.getUniformLocation(program, "lightAmbient"), flatten(lightAmbient));
    gl.uniform4fv(gl.getUniformLocation(program, "lightDiffuse"), flatten(lightDiffuse));
    gl.uniform4fv(gl.getUniformLocation(program, "lightDirection"), flatten(lightDirection));

    //Texture mapping of the sphere
    //Create the texture object
    var texture = gl.createTexture();

    //Populate filter modes arrays (both for handling magnification and minification issues)
    filterModesMag = [
        gl.NEAREST,
        gl.LINEAR,
    ];

    filterModesMin = [
        gl.NEAREST,
        gl.LINEAR,
        gl.NEAREST_MIPMAP_NEAREST,
        gl.LINEAR_MIPMAP_LINEAR,
        gl.NEAREST_MIPMAP_LINEAR,
        gl.LINEAR_MIPMAP_NEAREST
    ];

    //Create an image object
    //var image = new Image();
    var image = document.createElement('img');
    image.crossorigin = 'anonymous';
    //Register an event handler to be called when the image loading completes
    image.onload = function () {
        //Flip the image's y axis
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        //Enable texture unit 0
        gl.activeTexture(gl.TEXTURE0);
        //Bind the texture object to the target
        gl.bindTexture(gl.TEXTURE_2D, texture);
        //Set the texture image
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        //Set the texture parameters
        //1. Set the wrap mode to repeat (for both s and t coordinates)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

        //As the user can select minification techniques which make use of MipMap textures, it is necessary to generate MipMap textures beforehand
        gl.generateMipmap(gl.TEXTURE_2D);

        //2. Set the filter mode to nearest (for both handling magnification and minifications issues 
        // - even though the default value for the minification method should be gl.NEAREST_MIPMAP_LINEAR)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filterModesMag[0]);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filterModesMin[0]);

        //Set the texture linked to texture unit 0 as the sampler2D in the fragment shader
        gl.uniform1i(gl.getUniformLocation(program, "texMap"), 0);

        render();
    };

    //Tell the browser to load an image
    image.src = 'earth.jpg';

    //Calculate all the matrices that will remain the same throughout the program and the calls to the render() function,
    //so that it's not necessary to re-create them every time the render function is called. They are global variables for the program.
    //1. Model matrix
    modelMatrix = mat4();
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelMatrix"), false, flatten(modelMatrix));

    //2. Projection (perspective) matrix 
    projectionMatrix = mat4();
    var fovy = 90;
    var aspect = 1;
    var near = 0.001;
    var far = 100;
    projectionMatrix = perspective(fovy, aspect, near, far);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    //Calculate the view matrix
    var viewMatrix = mat4();
    //Rotate the camera around a circle with radius 2 centered on (0.0, 0.0, 0.0)
    eye = vec3(2.0 * Math.cos(theta), 0.0,  2.0 * Math.sin(theta));
    at = vec3(0.0, 0.0, 0.0);
    up = vec3(0.0, 1.0, 0.0); 
    viewMatrix = mult(mult(viewMatrix, modelMatrix), lookAt(eye, at, up));
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "viewMatrix"), false, flatten(viewMatrix));

    //Calculate the normal matrix
    var N = normalMatrix(mult(viewMatrix, modelMatrix), true);
    gl.uniformMatrix3fv(gl.getUniformLocation(program, "normalMatrix"), false, flatten(N));

    //Draw the sphere
    gl.drawArrays(gl.TRIANGLES, 0, vertices.length);

    //Slightly increase the rotation angle for the camera
    theta += 0.005;

    requestAnimationFrame(render);
}

//helper functions to draw the 1 unit sphere
function tetrahedron(a, b, c, d, n)
{
    divideTriangle(a, b, c, n);
    divideTriangle(d, c, b, n);
    divideTriangle(a, d, b, n);
    divideTriangle(a, c, d, n);
}

function divideTriangle(a, b, c, count)
{
    if (count > 0)
    {
        var ab = normalize(mix(a, b, 0.5), true);
        var ac = normalize(mix(a, c, 0.5), true);
        var bc = normalize(mix(b, c, 0.5), true);

        divideTriangle(a, ab, ac, count - 1);
        divideTriangle(ab, b, bc, count - 1);
        divideTriangle(bc, c, ac, count - 1);
        divideTriangle(ab, bc, ac, count - 1);
    }
    else
    {
        triangle(a, b, c);
    }
}

function triangle(a, b, c)
{
    vertices.push(a);
    vertices.push(b);
    vertices.push(c);

    normals.push(a[0], a[1], a[2], 0.0);
    normals.push(b[0], b[1], b[2], 0.0);
    normals.push(c[0],c[1], c[2], 0.0);
}

function setMenus() {
    //1. select magnification method
    var fmmag = document.getElementById("filter_mode_mag");
    fmmag.addEventListener("click", function () {
        var filterMagIndex = fmmag.selectedIndex;
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filterModesMag[filterMagIndex]);
    });

    //2. select minification method
    var fmmin = document.getElementById("filter_mode_min");
    fmmin.addEventListener("click", function () {
        var filterMinIndex = fmmin.selectedIndex;
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filterModesMin[filterMinIndex]);
    });
}


