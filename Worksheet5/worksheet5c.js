window.onload = function () {
    init();
}

//Variables linked to the loading of the OBJ model
var g_objDoc = null; // The information of OBJ file
var g_drawingInfo = null; // The information for drawing 3D model
var model = null;

function init()
{
    var canvas = document.getElementById("canv");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn�t available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    
    // Load shaders 
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //Enabling depth buffer and face culling
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);

    // Get the storage locations of attribute and uniform variables
    var a_program = program;
    a_program.vPosition = gl.getAttribLocation(program, 'vPosition');
    a_program.vNormal = gl.getAttribLocation(program, 'vNormal');
    
    // Prepare empty buffer objects for vertex coordinates and normals
    model = initVertexBuffers(gl, a_program);
    
    // Start reading the OBJ file
    readOBJFile('monkey.obj', gl, model, 1, false);

    //Set the model-view and projection matrices
    var modelViewMatrix = mat4();
    eye = vec3(-3, 0, 6);
    at = vec3(0.0, 0.0, 0.0);
    up = vec3(0.0, 1.0, 0.0);
    modelViewMatrix = mult(modelViewMatrix, lookAt(eye, at, up));
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "modelViewMatrix"), false, flatten(modelViewMatrix));

    var projectionMatrix = mat4();
    var perspectiveMatrix = perspective(90, 1, 0.001, 10);
    projectionMatrix = mult(projectionMatrix, perspectiveMatrix);
    gl.uniformMatrix4fv(gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    if (!g_drawingInfo && g_objDoc && g_objDoc.isMTLComplete())
    {
        // OBJ and all MTLs are available
        g_drawingInfo = onReadComplete(gl, model, g_objDoc);  
    }
    if (!g_drawingInfo)
    {
        requestAnimFrame(render);
        return;
    }
   

    gl.drawElements(gl.TRIANGLES, g_drawingInfo.indices.length, gl.UNSIGNED_SHORT, 0); //draw call for the loaded OBJ file
}

// Create a buffer object and perform the initial configuration
function initVertexBuffers(gl, program)
{
    var o = new Object();
    o.vertexBuffer = createEmptyArrayBuffer(gl, program.vPosition, 3, gl.FLOAT);
    o.normalBuffer = createEmptyArrayBuffer(gl, program.vNormal, 3, gl.FLOAT);
    o.indexBuffer = gl.createBuffer();
    return o;
}

function createEmptyArrayBuffer(gl, a_attribute, num, type)
{
    var buffer = gl.createBuffer(); // Create a buffer object
  
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);
    gl.enableVertexAttribArray(a_attribute); // Enable the assignment
   
    return buffer;
}

//Read a file
function readOBJFile(fileName, gl, model, scale, reverse) 
{
    var request = new XMLHttpRequest();
   
    request.onreadystatechange = function() 
    {
        if (request.readyState === 4 && request.status !== 404) 
        {
            onReadOBJFile(request.responseText, fileName, gl, model, scale, reverse);
         }
    }
    request.open('GET', fileName, true); // Create a request to get file
    request.send(); // Send the request
}



// OBJ file has been read
function onReadOBJFile(fileString, fileName, gl, o, scale, reverse) 
{
    var objDoc = new OBJDoc(fileName); // Create a OBJDoc object
    var result = objDoc.parse(fileString, scale, reverse);
    if (!result) 
    {
        g_objDoc = null; g_drawingInfo = null;
        console.log("OBJ file parsing error.");
        return;
    }
    g_objDoc = objDoc;
}

// OBJ File has been read completely
function onReadComplete(gl, model, objDoc)
{
    // Acquire the vertex coordinates and colors from OBJ file
    var drawingInfo = objDoc.getDrawingInfo();
    
     // Write date into the buffer object
     gl.bindBuffer(gl.ARRAY_BUFFER, model.vertexBuffer);
     gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.vertices,gl.STATIC_DRAW);
    
     gl.bindBuffer(gl.ARRAY_BUFFER, model.normalBuffer);
     gl.bufferData(gl.ARRAY_BUFFER, drawingInfo.normals, gl.STATIC_DRAW);
    
     // Write the indices to the buffer object
     gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.indexBuffer);
     gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, drawingInfo.indices, gl.STATIC_DRAW);
    
     return drawingInfo;
}
